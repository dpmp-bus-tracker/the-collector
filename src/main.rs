pub(crate) mod dpmp_api;
pub(crate) mod models;
pub(crate) mod schema;

use std::{env, time::Duration};

use anyhow::{anyhow, Context, Error, Result};
use chrono::Utc;
use diesel::{insert_into, Connection, PgConnection};
use diesel_async::{AsyncConnection, AsyncPgConnection, RunQueryDsl};
use diesel_migrations::MigrationHarness;
use diesel_migrations::{embed_migrations, EmbeddedMigrations};
use dotenv::dotenv;
use dpmp_api::fetch_current_buses;
use log::{error, info};
use models::batch::{self, Batch};
use models::bus::{self, Bus};
use schema::buses::dsl::buses as buses_table;
use tokio::time;

pub const MIGRATIONS: EmbeddedMigrations = embed_migrations!("migrations");

#[tokio::main]
async fn main() {
    env_logger::init();
    dotenv().ok();

    #[allow(clippy::unwrap_used)]
    let database_url = env::var("DATABASE_URL")
        .map_err(|_| anyhow!("DATABASE_URL must be set"))
        .unwrap();

    #[allow(clippy::unwrap_used)]
    let failure_post_url = env::var("FAILURE_POST_URL")
        .map_err(|_| anyhow!("FAILURE_POST_URL must be set"))
        .unwrap();

    #[allow(clippy::unwrap_used)]
    let interval_in_seconds: u64 = env::var("INTERVAL_IN_SECONDS")
        .map(|v| v.parse())
        .unwrap_or(Ok(15))
        .unwrap();

    #[allow(clippy::unwrap_used)]
    let mut connection = establish_connection(&database_url).unwrap();
    #[allow(clippy::unwrap_used)]
    connection.run_pending_migrations(MIGRATIONS).unwrap();

    #[allow(clippy::unwrap_used)]
    let mut connection = establish_async_connection(&database_url).await.unwrap();

    let mut interval = time::interval(Duration::from_secs(interval_in_seconds));

    loop {
        interval.tick().await;
        info!("starting update");
        if let Err(error) = update_buses(&mut connection).await {
            error!("update error: {:?}", error);
            let _ = notify_about_failure(&failure_post_url, error).await;
        }
    }
}

async fn establish_async_connection(database_url: &str) -> Result<AsyncPgConnection> {
    Ok(AsyncPgConnection::establish(database_url).await?)
}

fn establish_connection(database_url: &str) -> Result<PgConnection> {
    Ok(PgConnection::establish(database_url)?)
}

async fn update_buses(connection: &mut AsyncPgConnection) -> Result<()> {
    let buses = fetch_current_buses()
        .await
        .context("could not fetch current buses")?;

    let batch = Batch {
        id: None,
        timestamp: Utc::now().naive_utc(),
    };
    let batch_id = batch::insert_and_get_id(connection, &batch)
        .await
        .context("could not insert new buses into the database")?;

    info!("batch_id={batch_id}");

    let buses = buses
        .iter()
        .map(|dto| bus::from_dto(batch_id, dto))
        .collect::<Result<Vec<Bus>>>()?;

    upload_new_buses(connection, &buses).await?;
    Ok(())
}

async fn upload_new_buses(connection: &mut AsyncPgConnection, buses: &[Bus]) -> Result<()> {
    insert_into(buses_table)
        .values(buses)
        .execute(connection)
        .await?;
    Ok(())
}

async fn notify_about_failure(url: &str, error: Error) -> Result<()> {
    let client = reqwest::Client::new();
    client.post(url).body(format!("{error:?}")).send().await?;
    Ok(())
}

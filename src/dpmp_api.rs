use anyhow::{bail, Context, Result};

const DPMP_API_URL: &str = "https://online.dpmp.cz/api/buses";
const API_KEY: &str = "3e86570d-56a1-4ec1-8012-c1a9f98d18cc";

#[derive(serde::Serialize)]
struct DpmpApiRequest {
    key: String,
}

#[derive(serde::Deserialize)]
struct DpmpApiResponse {
    success: bool,
    data: Vec<Bus>,
}

#[derive(serde::Deserialize)]
pub struct Bus {
    pub vid: String,
    pub state_dtime: Option<String>,
    pub line_name: Option<String>,
    pub line_direction: Option<String>,
    pub destination_name: Option<String>,
    pub last_stop_number: Option<String>,
    pub last_stop_name: Option<String>,
    pub current_stop_number: Option<String>,
    pub current_stop_name: Option<String>,
    pub current_stop_scheduled_departure: Option<String>,
    pub gps_latitude: Option<f64>,
    pub gps_longitude: Option<f64>,
    pub time_difference: Option<String>,
    pub connection_no: Option<i32>,
    pub door: Option<bool>,
    pub gps_course: Option<i32>,
    pub gps_speed: Option<i32>,
    pub on_station: Option<bool>,
}

pub async fn fetch_current_buses() -> Result<Vec<Bus>> {
    let body = serde_json::to_string(&DpmpApiRequest {
        key: API_KEY.to_string(),
    })?;

    let client = reqwest::Client::new();
    let response = client
        .post(DPMP_API_URL)
        .body(body)
        .send()
        .await
        .context("request failed")?
        .error_for_status()
        .context("server responded with error")?;

    let data: DpmpApiResponse = response
        .json()
        .await
        .context("could not parse the JSON response")?;
    if !data.success {
        bail!("server responded with success = false");
    }

    Ok(data.data)
}

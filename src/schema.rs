// @generated automatically by Diesel CLI.

diesel::table! {
    batches (id) {
        id -> Int4,
        timestamp -> Timestamp,
    }
}

diesel::table! {
    buses (id) {
        id -> Int4,
        batch_id -> Int4,
        vid -> Int4,
        state_dtime -> Nullable<Timestamp>,
        line_name -> Nullable<Text>,
        line_direction -> Nullable<Text>,
        destination_name -> Nullable<Text>,
        last_stop_number -> Nullable<Int4>,
        last_stop_name -> Nullable<Text>,
        current_stop_number -> Nullable<Int4>,
        current_stop_name -> Nullable<Text>,
        current_stop_scheduled_departure -> Nullable<Time>,
        gps_latitude -> Nullable<Float8>,
        gps_longitude -> Nullable<Float8>,
        time_difference -> Nullable<Interval>,
        connection_no -> Nullable<Int4>,
        door -> Nullable<Bool>,
        gps_course -> Nullable<Int4>,
        gps_speed -> Nullable<Int4>,
        on_station -> Nullable<Bool>,
    }
}

diesel::joinable!(buses -> batches (batch_id));

diesel::allow_tables_to_appear_in_same_query!(
    batches,
    buses,
);

use crate::schema::batches::dsl::batches;
use anyhow::{Context, Result};
use chrono::NaiveDateTime;
use diesel::{insert_into, prelude::*};
use diesel_async::{AsyncPgConnection, RunQueryDsl};

sql_function!(
    fn last_insert_id() -> diesel::sql_types::Integer
);

#[derive(Insertable, Selectable, Queryable)]
#[diesel(table_name = crate::schema::batches)]
#[diesel(check_for_backend(diesel::pg::Pg))]
pub struct Batch {
    #[diesel(deserialize_as = i32)]
    pub id: Option<i32>,
    pub timestamp: NaiveDateTime,
}

pub async fn insert_and_get_id(connection: &mut AsyncPgConnection, batch: &Batch) -> Result<i32> {
    let inserted_batch: Batch = insert_into(batches)
        .values(batch)
        .returning(Batch::as_returning())
        .get_result::<Batch>(connection)
        .await?;

    inserted_batch
        .id
        .context("invalid response from database, expected Batch with an id")
}

use anyhow::{anyhow, bail, Context, Result};
use chrono::{NaiveDateTime, NaiveTime};
use diesel::{data_types::PgInterval, prelude::*};

use crate::dpmp_api;

#[derive(Insertable)]
#[diesel(table_name = crate::schema::buses)]
#[diesel(check_for_backend(diesel::pg::Pg))]
pub struct Bus {
    pub vid: i32,
    pub batch_id: i32,
    pub state_dtime: Option<NaiveDateTime>,
    pub line_name: Option<String>,
    pub line_direction: Option<String>,
    pub destination_name: Option<String>,
    pub last_stop_number: Option<i32>,
    pub last_stop_name: Option<String>, // known nullable, last_stop_number = 0
    pub current_stop_number: Option<i32>,
    pub current_stop_name: Option<String>,
    pub current_stop_scheduled_departure: Option<NaiveTime>,
    pub gps_latitude: Option<f64>,
    pub gps_longitude: Option<f64>,
    pub time_difference: Option<PgInterval>,
    pub connection_no: Option<i32>,
    pub door: Option<bool>,
    pub gps_course: Option<i32>,
    pub gps_speed: Option<i32>,
    pub on_station: Option<bool>,
}

pub fn from_dto(batch_id: i32, dto: &dpmp_api::Bus) -> Result<Bus> {
    Ok(Bus {
        vid: dto.vid.parse()?,
        batch_id,
        state_dtime: dto.state_dtime.as_ref().map(map_dto_datetime).transpose()?,
        line_name: dto.line_name.clone(),
        line_direction: dto.line_direction.clone(),
        destination_name: dto.destination_name.clone(),
        last_stop_number: dto
            .last_stop_number
            .clone()
            .map(|v| v.parse())
            .transpose()?,
        last_stop_name: dto.last_stop_name.clone(),
        current_stop_number: dto
            .current_stop_number
            .clone()
            .map(|v| v.parse())
            .transpose()?,
        current_stop_name: dto.current_stop_name.clone(),
        current_stop_scheduled_departure: dto
            .current_stop_scheduled_departure
            .as_ref()
            .map(map_dto_time)
            .transpose()?,
        gps_latitude: dto.gps_latitude,
        gps_longitude: dto.gps_longitude,
        time_difference: dto
            .time_difference
            .as_ref()
            .map(map_dto_interval)
            .transpose()?,
        connection_no: dto.connection_no,
        door: dto.door,
        gps_course: dto.gps_course,
        gps_speed: dto.gps_speed,
        on_station: dto.on_station,
    })
}

#[allow(clippy::ptr_arg)]
fn map_dto_datetime(dto_datetime: &String) -> Result<NaiveDateTime> {
    NaiveDateTime::parse_from_str(dto_datetime, "%Y-%m-%d %H:%M:%S%.f")
        .with_context(|| anyhow!("could not parse datetime: {dto_datetime}"))
}

#[allow(clippy::ptr_arg)]
fn map_dto_time(dto_time: &String) -> Result<NaiveTime> {
    NaiveTime::parse_from_str(dto_time, "%H:%M:%S")
        .with_context(|| anyhow!("could not parse time: {dto_time}"))
}

#[allow(clippy::ptr_arg)]
fn map_dto_interval(dto_interval: &String) -> Result<PgInterval> {
    let mut dto_interval = dto_interval.as_str();

    let sign = dto_interval
        .chars()
        .next()
        .ok_or(anyhow!("invalid interval format: {dto_interval}"))?;
    let mut multiplier = 1;
    if sign == '-' {
        multiplier = -1;
        dto_interval = &dto_interval[1..];
    }

    let parts: Vec<i64> = dto_interval
        .split(':')
        .map(|v| Ok(v.parse()?))
        .collect::<Result<_>>()?;
    if parts.len() != 3 {
        bail!("invalid interval format: {dto_interval}");
    }

    let microseconds = ((60 * parts[0] + parts[1]) * 60 + parts[2]) * 1_000_000 * multiplier;
    Ok(PgInterval::new(microseconds, 0, 0))
}

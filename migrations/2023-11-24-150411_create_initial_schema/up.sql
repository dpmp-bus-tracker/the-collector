
CREATE TABLE batches (
  id        SERIAL PRIMARY KEY,
  timestamp TIMESTAMP NOT NULL
);

CREATE TABLE buses (
  id                                SERIAL PRIMARY KEY,
  batch_id                          SERIAL NOT NULL,
  CONSTRAINT fk_batches
    FOREIGN KEY(batch_id) 
      REFERENCES batches(id),
  vid                               INT NOT NULL,
  state_dtime                       TIMESTAMP,
  line_name                         TEXT,
  line_direction                    TEXT,
  destination_name                  TEXT,
  last_stop_number                  INT,
  last_stop_name                    TEXT,
  current_stop_number               INT,
  current_stop_name                 TEXT,
  current_stop_scheduled_departure  TIME,
  gps_latitude                      DOUBLE PRECISION,
  gps_longitude                     DOUBLE PRECISION,
  time_difference                   INTERVAL,
  connection_no                     INT,
  door                              BOOLEAN,
  gps_course                        INT,
  gps_speed                         INT,
  on_station                        BOOLEAN
);

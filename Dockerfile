FROM docker.io/library/rust:1 as builder

ADD . /build
WORKDIR /build
RUN cargo build --release


FROM docker.io/library/debian:12-slim

RUN apt-get update \
  && apt-get install -y ca-certificates libpq5 tzdata \
  && rm -rf /var/lib/apt/lists/*

ENV TZ=Etc/UTC

RUN mkdir -p /app

COPY --from=builder /build/target/release/dpmp-bus-tracker /app/dpmp-bus-tracker

WORKDIR /app

CMD ["./dpmp-bus-tracker"]
